# Run Database for the BPC-BDS course

Clone the repository and run the following command in the project root directory.
```shell
$ docker-compose up
```

To run it as a daemon, suffix the above command with the `-d` parameter.
