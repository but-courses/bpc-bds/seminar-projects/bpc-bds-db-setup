CREATE TABLESPACE "data1" LOCATION '/db-data1/postgresql';
CREATE TABLESPACE "data2" LOCATION '/db-data2/postgresql';

SET default_tablespace = "data1";